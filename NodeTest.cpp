/**
 * @file   NodeTest.cpp
 * @author Jim Daehn
 * @brief  TODO: Give brief description of this class.
 */

#include "NodeTest.h"

CPPUNIT_TEST_SUITE_REGISTRATION(NodeTest);

NodeTest::NodeTest() :
		defaultNode(nullptr), coursePrefixNode(nullptr), courseNameNode(nullptr), itemNextNode(nullptr) {
	// intentionally empty
}

NodeTest::~NodeTest() {

}

void NodeTest::setUp() {
	defaultNode = new Node<std::string>();
	coursePrefixNode = new Node<std::string>(COURSE_PREFIX);
	courseNameNode = new Node<std::string>(COURSE_NAME);
	itemNextNode = new Node<std::string>(COURSE_PREFIX, courseNameNode);
}

void NodeTest::tearDown() {
	delete defaultNode;
	defaultNode = nullptr;

	delete coursePrefixNode;
	coursePrefixNode = nullptr;

	delete courseNameNode;
	courseNameNode = nullptr;

	delete itemNextNode;
	itemNextNode = nullptr;
}

void NodeTest::testDefaultNodeHasNoNextNode() {
	CPPUNIT_ASSERT_MESSAGE(MESSAGE, defaultNode->getNext() == nullptr);
}

void NodeTest::testSingleParamNodeHasNoNextNode() {
	CPPUNIT_ASSERT_MESSAGE(MESSAGE, coursePrefixNode->getNext() == nullptr);
}

void NodeTest::testTwoParamNodeHasNonNullNextNode() {
	CPPUNIT_ASSERT_MESSAGE(MESSAGE, itemNextNode->getNext() != nullptr);
}
