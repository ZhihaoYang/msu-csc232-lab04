#define FALSE 0
#define TRUE !FALSE
#define DEMO FALSE

#if DEMO

#include <cstdlib>
#include <iostream>
#include <string>
#include "BagInterface.h"
#include "LinkedBag.h"

template<typename ItemType>
void printBag(BagInterface<ItemType>* bag) {
	std::cout << "[ ";
	for (auto item : bag->toVector()) {
		std::cout << item << " ";
	}
	std::cout << "]" << std::endl;
}

int main(int argc, char** argv) {
	BagInterface<std::string> *thisBag = new LinkedBag<std::string>();
	thisBag->add("a");
	thisBag->add("b");
	thisBag->add("c");

	BagInterface<std::string> *thatBag = new LinkedBag<std::string>();
	thatBag->add("b");
	thatBag->add("b");
	thatBag->add("d");
	thatBag->add("e");

	BagInterface<std::string> *unionBag;
	unionBag = thisBag->getUnionWithBag(thatBag);
	std::cout << "myBag->isEmpty() = " << (unionBag->isEmpty() ? "true" : "false") << std::endl;
	printBag<std::string>(unionBag);

	BagInterface<std::string> *intersectionBag;
	intersectionBag = thisBag->getIntersectionWithBag(thatBag);
	std::cout << "myBag->isEmpty() = " << (intersectionBag->isEmpty() ? "true" : "false") << std::endl;
	printBag<std::string>(intersectionBag);

	BagInterface<std::string> *differenceBag;
	differenceBag = thisBag->getDifferenceWithBag(thatBag);
	std::cout << "myBag->isEmpty() = " << (differenceBag->isEmpty() ? "true" : "false") << std::endl;
	printBag<std::string>(differenceBag);

	return EXIT_SUCCESS;
}

#else

#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>

#include <cppunit/Test.h>
#include <cppunit/TestFailure.h>
#include <cppunit/portability/Stream.h>

class ProgressListener: public CPPUNIT_NS::TestListener {
public:

	ProgressListener() :
	m_lastTestFailed(false) {
	}

	~ProgressListener() {
	}

	void startTest(CPPUNIT_NS::Test *test) {
		CPPUNIT_NS::stdCOut() << test->getName();
		CPPUNIT_NS::stdCOut() << "\n";
		CPPUNIT_NS::stdCOut().flush();

		m_lastTestFailed = false;
	}

	void addFailure(const CPPUNIT_NS::TestFailure &failure) {
		CPPUNIT_NS::stdCOut() << " : "
		<< (failure.isError() ? "error" : "assertion");
		m_lastTestFailed = true;
	}

	void endTest(CPPUNIT_NS::Test *test) {
		if (!m_lastTestFailed)
		CPPUNIT_NS::stdCOut() << " : OK";
		CPPUNIT_NS::stdCOut() << "\n";
	}

private:
	// Prevents the use of the copy constructor.
	ProgressListener(const ProgressListener &copy);

	// Prevents the use of the copy operator.
	void operator=(const ProgressListener &copy);

private:
	bool m_lastTestFailed;
};

int main() {
	// Create the event manager and test controller
	CPPUNIT_NS::TestResult controller;

	// Add a listener that collects test result
	CPPUNIT_NS::TestResultCollector result;
	controller.addListener(&result);

	// Add a listener that print dots as test run.
	ProgressListener progress;
	controller.addListener(&progress);

	// Add the top suite to the test runner
	CPPUNIT_NS::TestRunner runner;
	runner.addTest(CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest());
	runner.run(controller);

	// Print test in a compiler compatible format.
	CPPUNIT_NS::CompilerOutputter outputter(&result, CPPUNIT_NS::stdCOut());
	outputter.write();

	return result.wasSuccessful() ? 0 : 1;
}

#endif
